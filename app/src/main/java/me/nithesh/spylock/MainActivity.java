package me.nithesh.spylock;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;


public class MainActivity extends Activity {

    EditText passwordEditText;
    private int counter = 0; //count wrong password
    SharedPreferences userpref;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Passcode = "PasscodeKey";
    public static final String Email = "emailKey";
    private static final String Hint = "hintKey";
    ImageButton hintButton;
    ImageButton SettingsButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Set up our Lockscreen
        makeFullScreen();
        startService(new Intent(this, LockScreenService.class));
        userpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        if (userpref.contains(Passcode) && userpref.contains(Email)) {
            userpref.getString(Passcode, "");
            userpref.getString(Email, "");

        } else {
            Intent i = new Intent(this, FirstSettingsActivity.class);
            startActivity(i);
        }

        setContentView(R.layout.activity_main);

        passwordEditText = (EditText) findViewById(R.id.passwordEditText0);

        addsettingListener();
        addhintListener();


    }

    @Override
    protected void onResume() {
        super.onResume();
        counter = 0;
    }

    /**
     * A simple method that sets the screen to fullscreen.  It removes the Notifications bar,
     * the Actionbar and the virtual keys (if they are on the phone)
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void makeFullScreen() {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT < 19) { //View.SYSTEM_UI_FLAG_IMMERSIVE is only on API 19+
            this.getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        } else {
            this.getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE);
        }
    }

    @Override
    public void onBackPressed() {
        return; //Do nothing!
    }

    public void unlockButtonClicked(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(passwordEditText.getWindowToken(), 0);

        String p = userpref.getString(Passcode, "");

        if (passwordEditText.getText().toString().equals(p)) {
            unlockScreen();
        } else if (passwordEditText.getText().toString().equals(p) == false && counter > 0) {
            sendAlert();
        } else {
            Toast.makeText(getApplicationContext(), "Wrong password".toString(), Toast.LENGTH_SHORT).show();
            counter++;
        }

    }

    /////////////////////////////////////////////////////////////
    /////////////////// Unlock Screen ///////////////////////////
    public void unlockScreen() {
        //Instead of using finish(), this totally destroys the process
        android.os.Process.killProcess(android.os.Process.myPid());
    }


    /////////////////////////////////////////////////////////////
    //////////////////////// Alert //////////////////////////////
    public void sendAlert() {


        stopService(new Intent(this, LockScreenService.class));

        Intent i = new Intent(this, AlertActivity.class);
        startActivity(i);

    }


    /////////////////////////////////////////////////////////////
    //////////////////////// ChangeSetting //////////////////////
    public void addsettingListener() {

        SettingsButton = (ImageButton) findViewById(R.id.SettingsButton);

        SettingsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent i = new Intent(MainActivity.this, ChangeSettingActivity.class);
                startActivity(i);

            }

        });


    }

    public void addhintListener() {

        hintButton = (ImageButton) findViewById(R.id.hintButton);

        hintButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Toast.makeText(MainActivity.this, userpref.getString(Hint, ""), Toast.LENGTH_SHORT).show();

            }

        });

    }



}


