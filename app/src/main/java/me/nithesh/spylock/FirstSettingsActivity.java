package me.nithesh.spylock;

/**
 * Created by nithesh on 4/26/2015.
 */
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class FirstSettingsActivity extends Activity {

    private EditText passcodeEditText;
    private EditText emailEditText;
    private EditText hintEditText;

    private SharedPreferences userpref;
    public static final String MyPREFERENCES = "MyPrefs" ;

    private static final String Passcode = "PasscodeKey";
    private static final String Email = "emailKey";
    private static final String Hint = "hintKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_settings);

        userpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        passcodeEditText = (EditText) findViewById(R.id.passcodeEditText1);
        emailEditText = (EditText) findViewById(R.id.emailEditText1);
        hintEditText = (EditText) findViewById(R.id.hintEditText1);


    }

    public void submitButton1Clicked(View view){

        if(passcodeEditText.getText().toString().equals("") ||
                emailEditText.getText().toString().equals("") ||
                hintEditText.getText().toString().equals("")){

            Toast.makeText(this, "Please Enter All Fields. ", Toast.LENGTH_SHORT).show();
        }
        else {

            String p = passcodeEditText.getText().toString();
            String e = emailEditText.getText().toString();
            String h = hintEditText.getText().toString();

            Editor editor = userpref.edit();
            editor.putString(Passcode, p);
            editor.putString(Email, e);
            editor.putString(Hint, h);

            editor.commit();

            finish();
        }

    }

}
