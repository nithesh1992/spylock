package me.nithesh.spylock;

/**
 * Created by nithesh on 4/26/2015.
 */

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class AlertActivity extends Activity {
    //--------GPS variables------//
    private LocationManager mLocationManager;
    private Location mLocation;
    private double latitude;
    private double longitude;

    //-------Camera variables----//
    private Camera mCamera;
    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    private Context myContext;
    private LinearLayout cameraPreview;
    private boolean cameraFront = false;
    private EditText mTextView;

    //private String timeStamp;
    private SharedPreferences userpref;
    public static final String MyPREFERENCES = "MyPrefs" ;
    private static final String Passcode = "PasscodeKey";
    private static final String Email = "emailKey";
    private static final String Hint = "hintKey";

    Button OKButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);



        //--------GPS ------//
        GPSTracker gps = new GPSTracker(this);
        if(gps.canGetLocation()){

            latitude = gps.getLatitude();
            longitude = gps.getLongitude();


        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
          //  gps.showSettingsAlert();
        }



        //-------Camera ----//
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        myContext = this;
        initialize();


        userpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //countdown();
//        new CountDownTimer(2500, 1000) {
//            public void onFinish() {
//
//                OKButton.performClick();
//            }
//
//            public void onTick(long millisUntilFinished) {
//            }
//
//        }.start();




    }

    ////////////////////////////////////////////////////
    ///////////////////Camera///////////////////////////

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public void onResume() {
        super.onResume();
        if (!hasCamera(myContext)) {
            Toast toast = Toast.makeText(myContext, "Sorry, your phone does not have a camera!", Toast.LENGTH_LONG);
            toast.show();
            finish();
        }
        if (mCamera == null) {
            //if the front facing camera does not exist
            if (findFrontFacingCamera() < 0) {
                Toast.makeText(this, "No front facing camera found.", Toast.LENGTH_LONG).show();
                //switchCamera.setVisibility(View.GONE);
            }
            //mCamera = Camera.open(findBackFacingCamera());
            mCamera = Camera.open(findFrontFacingCamera());
            mPicture = getPictureCallback();
            mPreview.refreshCamera(mCamera);
        }




    }

    @Override
    public void onBackPressed() {
        //do nothing
    }


    @Override
    protected void onPause() {
        super.onPause();
        //when on Pause, release camera in order to be used from other applications
        releaseCamera();
    }

    private boolean hasCamera(Context context) {
        //check if the device has camera
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    private Camera.PictureCallback getPictureCallback() {
        Camera.PictureCallback picture = new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                //make a new picture file
                File pictureFile = getOutputMediaFile();

                if (pictureFile == null) {
                    return;
                }
                try {
                    //write the file
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    fos.write(data);
                    fos.close();
                  //  Toast toast = Toast.makeText(myContext, "Picture saved: " + pictureFile.getName(), Toast.LENGTH_LONG);
                  //  toast.show();

                } catch (FileNotFoundException e) {
                } catch (IOException e) {
                }

                //refresh camera to continue preview
                mPreview.refreshCamera(mCamera);
            }
        };
        return picture;
    }

    //make picture and save to a folder
    private static File getOutputMediaFile() {
        //make a new file directory inside the "sdcard" folder
        File mediaStorageDir = new File("/sdcard/DCIM", "SPY Lock");

        //if this "SPY Lock folder does not exist
        if (!mediaStorageDir.exists()) {
            //if you cannot make this folder return
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        //take the current timeStamp
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        //and make a media file:
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    private void releaseCamera() {
        // stop and release camera
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    public void initialize() {
        cameraPreview = (LinearLayout) findViewById(R.id.camera_preview);
        mPreview = new CameraPreview(myContext, mCamera);
        cameraPreview.addView(mPreview);
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    private int findFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;
    }




    public void OKButtonClicked(View view){
        new AsyncTask<Void, Void, Void>() {
            @Override public Void doInBackground(Void... arg) {
                try {
                    mCamera.takePicture(null, null, mPicture);
                    //Toast.makeText(getApplicationContext(), "Take Picture " , Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    Log.e("Take Pic", e.getMessage(), e);
                }
                return null;
            }
        }.execute();

        new AsyncTask<Void, Void, Void>() {
            @Override public Void doInBackground(Void... arg) {
                try {
                    String file = getOutputMediaFile().getCanonicalPath() ;
                    String serviceName = Context.TELEPHONY_SERVICE;
                    TelephonyManager m_telephonyManager = (TelephonyManager) getSystemService(serviceName);
                    String IMEI,IMSI;
                    IMEI = m_telephonyManager.getDeviceId();
                    IMSI = m_telephonyManager.getSubscriberId();
                    SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMMM d 'at' hh:mm a 'in the year' yyyy G");
                    String time = formatter.format(new Date());
                    String Locationmsg = new StringBuilder()
                            .append("Time: "+time+"\n")
                            .append("Device IMEI: "+IMEI+"\n")
                            .append("Device IMSI: "+IMSI+"\n")
                            .append("Suspicious Smartphone Unlock Activity Detected at: \n"+latitude+","+longitude)
                            .append("\n See on the Map here https://www.google.com/maps/@"+latitude+","+longitude+",180m/data=!3m1!1e3 ")
                            .toString();

                    SendEmail.SendGmail(userpref.getString(Email,""), Locationmsg,file);

                } catch (Exception e) {
                    Log.e("SendMail", e.getMessage(), e);
                }
                return null;
            }
        }.execute();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                finish();
            }
        }, 1000);

    }

}

