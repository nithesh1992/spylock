package me.nithesh.spylock;

/**
 * Created by nithesh on 4/26/2015.
 */
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ChangeSettingActivity extends Activity {

    private EditText oldPasscodeEditText;
    private EditText newPasscodeEditText;
    private EditText hintEditText;
    private EditText emailEditText;

    SharedPreferences userpref;
    public static final String MyPREFERENCES = "MyPrefs" ;
    private static final String Passcode = "PasscodeKey";
    private static final String Email = "emailKey";
    private static final String Hint = "hintKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_setting);



        oldPasscodeEditText = (EditText)findViewById(R.id.oldPasscodeEditText2);
        newPasscodeEditText = (EditText)findViewById(R.id.newPasscodeEditText2);
        hintEditText = (EditText)findViewById(R.id.hintEditText2);
        emailEditText = (EditText)findViewById(R.id.emailEditText2);

        userpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

    }


    public void submitButton2Clicked(View view){

        if( oldPasscodeEditText.getText().toString().equals("") ||
                newPasscodeEditText.getText().equals("") ||
                hintEditText.getText().equals("") ||
                emailEditText.getText().equals("") )
        {
            Toast.makeText(this,"Please Enter All Fields.", Toast.LENGTH_SHORT).show();
        }
        else{
            //Check OLD Passcode
            String oldPasscodeInput = oldPasscodeEditText.getText().toString();

            //Retrieving values stored on the phone
            if(userpref.contains(Passcode)) {
                String storedPasscode = userpref.getString(Passcode, "");
                if(storedPasscode.toString().equals(oldPasscodeInput)){

                    String p = newPasscodeEditText.getText().toString();
                    String e = emailEditText.getText().toString();
                    String h = hintEditText.getText().toString();

                    SharedPreferences.Editor editor = userpref.edit();
                    editor.putString(Passcode, p);
                    editor.putString(Email, e);
                    editor.putString(Hint, h);
                    editor.commit();

                    Toast.makeText(this,"Your Info. has Changed.", Toast.LENGTH_SHORT).show();

                    finish();

                }else{

                    Toast.makeText(this,"OLD Passcode is not correct.", Toast.LENGTH_SHORT).show();
                }

            }


        }


    }


}

